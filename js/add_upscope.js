(function ($) {
  Drupal.behaviors.upscope = {
    attach: function (context, settings) {
      // Don't try to load upscope if we don't have an app id yet.
      if (settings.upscope.id !== null) {
        // This code is just a beautified version of what Upscope provides
        // So that we can modify the source URL and inject user data from Drupal.
        (function(w, u, d) {
          if (typeof u !== "function") {
            var i = function() {
              i.c(arguments)
            };
            i.q = [];
            i.c = function(args) {
              i.q.push(args)
            };
            w.Upscope = i;
            var l = function() {
              var s = d.createElement('script');
              s.type = 'text/javascript';
              s.async = true;

              s.src = 'https://code.upscope.io/' + settings.upscope.id + '.js';
              var x = d.getElementsByTagName('script')[0];
              x.parentNode.insertBefore(s, x);
            };
            l();
          }
        })(window, window.Upscope, document);

        if (settings.upscope.userData === null) {
          settings.upscope.userData = {
            uniqueId: '',
            identities: [],
          }
        }
        // Initialize upscope with data passed from Drupal to identify user.
        Upscope('init', {
          uniqueId: settings.upscope.userData.uniqueId,
          identities: settings.upscope.userData.identities,
          lookupCodeElement: "#upscope-support-code",
        });
      }
    }
  };
})(jQuery);
